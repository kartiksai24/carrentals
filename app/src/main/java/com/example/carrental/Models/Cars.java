package com.example.carrental.Models;

public class Cars {
    private int id;
    private String title,shortdesc;
    private String registration;
    private int image;
    private int colorimage;

    public Cars(int id, String title, String shortdesc, String registration, int image, int colorimage) {
        this.id = id;
        this.title = title;
        this.shortdesc = shortdesc;
        this.registration = registration;
        this.image = image;
        this.colorimage=colorimage;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public void setShortdesc(String shortdesc) {
        this.shortdesc = shortdesc;
    }

    public String getregistration() {
        return registration;
    }

    public void setregistration(String price) {
        this.registration = price;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getcolorImage() {
        return colorimage;
    }

    public void setcolorImage(int colorimage) {
        this.colorimage = colorimage;
    }

}
