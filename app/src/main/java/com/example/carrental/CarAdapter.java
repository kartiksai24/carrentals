package com.example.carrental;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carrental.Models.Cars;

import java.util.List;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.CarViewHolder> {

    private Context mCtx;
    private List<Cars> carsList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position,String textViewTitle, String textViewDesc);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public CarAdapter(Context mCtx, List<Cars> carsList) {
        this.mCtx = mCtx;
        this.carsList = carsList;
    }

    @NonNull
    @Override
    public CarViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(mCtx);
        View view=inflater.inflate(R.layout.list_layout,null);
        CarViewHolder carViewHolder=new CarViewHolder(view,mListener);
        return carViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CarViewHolder carViewHolder, int i) {

        Cars cars=carsList.get(i);

        carViewHolder.textViewTitle.setText(cars.getTitle());
        carViewHolder.textViewDesc.setText(cars.getShortdesc());
        carViewHolder.textViewRegistration.setText(cars.getregistration());
        carViewHolder.imageViewColor.setImageDrawable(mCtx.getResources().getDrawable(cars.getcolorImage(),null));
        carViewHolder.imageView.setImageDrawable(mCtx.getResources().getDrawable(cars.getImage(),null));

    }

    @Override
    public int getItemCount() {
        return carsList.size();
    }

    class CarViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView,imageViewColor;
        TextView textViewTitle, textViewDesc, textViewRegistration;
        
       public CarViewHolder(@NonNull View itemView, final OnItemClickListener listener
       ) {
           super(itemView);

           imageView=itemView.findViewById(R.id.imageView);
           imageViewColor=itemView.findViewById(R.id.colorSolid);
           textViewTitle=itemView.findViewById(R.id.textViewTitle);
           textViewDesc=itemView.findViewById(R.id.textViewShortDesc);
           textViewRegistration=itemView.findViewById(R.id.textViewRegistration);

           itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   if (listener != null) {
                       int position = getAdapterPosition();
                       if (position != RecyclerView.NO_POSITION) {

                           listener.onItemClick(position,String.valueOf(textViewTitle.getText()),String.valueOf(textViewDesc.getText()));
                       }
                   }
               }
           });
       }


    }

}
