package com.example.carrental;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;

import com.example.carrental.Models.Cars;

import java.util.ArrayList;
import java.util.List;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    CarAdapter carAdapter;
    List<Cars> carsList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        carsList=new ArrayList<>();

        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        carsList.add(
                new Cars(
                        1,
                        "Mazda-3",
                        "Intermediate 2/4 Door, FWD, Automatic Transmission",
                        "MKY 2341",
                        R.drawable.mazda3white,
                        R.drawable.red
                        ));

        carsList.add(
                new Cars(
                        1,
                        "Chevrolet Malibu",
                        "Fullsize 2/4 Door, FWD, Automatic Transmission",
                        "ABC 341",
                        R.drawable.malibuww,
                        R.drawable.white
                        ));

        carsList.add(
                new Cars(
                        1,
                        "Chrysler Pacifica",
                        "Mini Passenger Van, AWD, Automatic Transmission",
                        "ZXT 567",
                        R.drawable.pacificaww,
                        R.drawable.grey
                        ));

        carsList.add(
                new Cars(
                        1,
                        "GMC Yuckon",
                        "Full size SUV, AWD, Automatic Transmission",
                        "JTV 853",
                        R.drawable.mazda3white,
                        R.drawable.red
                        ));
        carsList.add(
                new Cars(
                        1,
                        "Mazda 3",
                        "Intermediate 2/4 Door, FWD, Automatic Transmission",
                        "MKY 2341",
                        R.drawable.mazda3white,
                        R.drawable.red
                        ));

        carsList.add(
                new Cars(
                        1,
                        "Chevrolet Malibu",
                        "Fullsize 2/4 Door, FWD, Automatic Transmission",
                        "ABC 341",
                        R.drawable.malibuwhite,
                        R.drawable.white
                        ));

        carsList.add(
                new Cars(
                        1,
                        "Chrysler Pacifica",
                        "Mini Passenger Van, AWD, Automatic Transmission",
                        "ZXT 567",
                        R.drawable.pacificaww,
                        R.drawable.grey
                        ));

        //creating recyclerview adapter
         carAdapter = new CarAdapter(this, carsList);

        //setting adapter to recyclerview
        recyclerView.setAdapter(carAdapter);

        carAdapter.setOnItemClickListener(new CarAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position,String textViewTitle, String textViewDesc) {
                carsList.get(position);
                Log.i("Position", String.valueOf(position));
                Log.i("Title", textViewTitle);

                Intent cardetail_intent = new Intent(MainActivity.this, CarDetails.class);
                cardetail_intent.putExtra("Title",textViewTitle);
                cardetail_intent.putExtra("Description",textViewDesc);

                startActivity(cardetail_intent);
            }
        });
    }
}
